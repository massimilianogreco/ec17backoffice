package com.softengunina.ec17;

import com.softengunina.ec17.payload.Image;
import org.junit.Test;
import static org.junit.Assert.*;

public class ImageUnitTest {
    
    @Test
    public void testImage1() {
        boolean result = Image.isExtensionValid(null);
        boolean expectedResult = false;
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testImage2() {
        boolean result = Image.isExtensionValid("");
        boolean expectedResult = false;
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testImage3() {
        boolean result = Image.isExtensionValid("file");
        boolean expectedResult = false;
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testImage4() {
        boolean result = Image.isExtensionValid("C:\\file.txt");
        boolean expectedResult = false;
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testImage5() {
        boolean result = Image.isExtensionValid("C:\\Users\\Utente\\file.jpg");
        boolean expectedResult = true;
        assertEquals(expectedResult, result);
    }
}
