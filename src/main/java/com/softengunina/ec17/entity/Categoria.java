package com.softengunina.ec17.entity;


public enum Categoria {
    INFORMATICA("Informatica"),
    VIDEOGIOCHI("Videogiochi"),
    ABBIGLIAMENTO("Abbigliamento"),
    LIBRI("Libri");

    private String type;

    private Categoria(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getValue(){
        return this.type.toUpperCase();
    }
}