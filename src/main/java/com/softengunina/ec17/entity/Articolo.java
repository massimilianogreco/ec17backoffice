package com.softengunina.ec17.entity;


import java.util.UUID;

public class Articolo{


    private UUID id;
    private String nome;
    private Categoria categoria;
    private double prezzo;
    private String descrizione;
    private String foto;
    private int quantita;
	
    public Articolo() {
    }

    public Articolo(String nome, Categoria categoria, double prezzo, String descrizione, int quantita, String foto) {
        this.nome = nome;
        this.categoria = categoria;
        this.prezzo = prezzo;
        this.descrizione = descrizione;
        this.foto = foto;
        this.quantita = quantita;
    }

    public int getQuantita()
    {
        return quantita;
    }
    public void setQuantita(int qt)
    {
        this.quantita=qt;
    }
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "ArticoloEntity{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", categoria=" + categoria +
                ", prezzo=" + prezzo +
                ", descrizione='" + descrizione + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }
}
