package com.softengunina.ec17.payload;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import org.apache.commons.io.FilenameUtils;

public class Image implements Serializable {
    
    static final String[] imageExtensions = {
        "bmp", "cdp", "djvu", "djv", "eps", "gif",
        "gpd", "jpd", "jpg", "jpeg", "pict", "png",
        "tga", "tiff", "pcx", "psd", "webp"            
    };
    
    private String name;
    private String ext;
    private int size;
    private byte[] image;

    public Image() {
    }

    public Image(String name, String ext, int size, byte[] image) {
        this.name = name;
        this.ext = ext;
        this.size = size;
        this.image = image;
    }
    
    public static Image fromPathToImage(String path) {
        Image image = new Image();
        image.setName(FilenameUtils.getBaseName(path));
        image.setExt(FilenameUtils.getExtension(path));
        try{
           
            byte dataBuffer[] = new byte[1024];
            byte bytesImage[];
            int bytesRead, totBytesRead = 0;
            FileInputStream in = new FileInputStream(path);
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                totBytesRead += bytesRead;
            }
            in.close();
            bytesImage = new byte[totBytesRead + 1024];
            in = new FileInputStream(path);
            bytesRead = 0;
            while ((bytesRead += in.read(bytesImage, bytesRead, 1024)) != -1) {}
            in.close();
            image.setSize(totBytesRead);
            image.setImage(bytesImage);

        }catch (IOException a){}
        return image;
    }
    
    public static boolean isExtensionValid(String path){
        String pathExt = FilenameUtils.getExtension(path);
        for(String ext : imageExtensions)
            if(ext.equals(pathExt))
                return true;
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Image{" +
                "name='" + name + '\'' +
                ", ext='" + ext + '\'' +
                ", size=" + size +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}