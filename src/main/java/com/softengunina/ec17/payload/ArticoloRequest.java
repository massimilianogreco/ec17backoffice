package com.softengunina.ec17.payload;

import com.softengunina.ec17.entity.Categoria;
import java.util.UUID;

public class ArticoloRequest{

    private UUID id;
    private String nome;
    private Categoria categoria;
    private double prezzo;
    private String descrizione;
    private Image foto;
    private int quantita;
	
    public ArticoloRequest() {
    }

    public ArticoloRequest(String nome, Categoria categoria, double prezzo, String descrizione, Image foto, int quantita) {
        this.nome = nome;
        this.categoria = categoria;
        this.prezzo = prezzo;
        this.descrizione = descrizione;
        this.foto = foto;
        this.quantita = quantita;
    }
    public int getQuantita()
    {
        return quantita;
    }
    public void setQuantita(int qt)
    {
        this.quantita=qt;
    }
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Image getFoto() {
        return foto;
    }

    public void setFoto(Image foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "ArticoloEntity{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", categoria=" + categoria +
                ", prezzo=" + prezzo +
                ", descrizione='" + descrizione + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }
}
